package com.javatechie.spring.soap.api;

import com.javatechie.spring.soap.api.client.SoapClient;
import com.javatechie.spring.soap.api.loaneligibility.Acknowledgement;
import com.javatechie.spring.soap.api.loaneligibility.CustomerRequest;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.StringWriter;

@SpringBootApplication
@RestController
public class SpringBootSoapWsClientApplication {
    @Autowired
    private SoapClient client;

    @PostMapping(value = "/getLoanStatus")
    public Acknowledgement invokeSoapClientToGetLoanStatus(@RequestBody CustomerRequest request) {
        return client.getLoanStatus(request);
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSoapWsClientApplication.class, args);
    }
}
